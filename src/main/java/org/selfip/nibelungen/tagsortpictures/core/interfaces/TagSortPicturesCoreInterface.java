package org.selfip.nibelungen.tagsortpictures.core.interfaces;

public interface TagSortPicturesCoreInterface {

	/**
	 * 
	 */
	public static final String PARAM_FOLDER_NAME_NOTAG = "param.folder.name.notag";
	
	/**
	 * 
	 */
	public static final String MSG_TAG_EXIST = "msg.tag.exist";
	
	/**
	 * 
	 */
	public static final String MSG_START_SCAN ="msg.start.scan";
	
	/**
	 * 
	 */
	public static final String MSG_END_SCAN = "msg.end.scan";
	
	/**
	 * 
	 */
	public static final String MSG_ERROR = "msg.error";
	
	/**
	 * 
	 */
	public static final String MSG_LOAD_TAG = "msg.load.tag";
	
	/**
	 * 
	 */
	public static final String MSG_PATH_FULL_PICTURE = "msg.path.full.picture";
		
	/**
	 * 
	 */
	public static final String MSG_ERROR_FOLDER_SOURCE = "msg.error.folder.source";
	
	/**
	 * 
	 */
	public static final String MSG_ERROR_FOLDER_DESTINATION = "msg.error.folder.destination";

	/**
	 * 
	 */
	public static final String MSG_PRE_SOURCE_FOLDER= "msg.pre.source.folder";
	
	/**
	 * 
	 */
	public static final String MSG_PRE_DESTINATION_FOLDER = "msg.pre.destination.folder";
	
	/**
	 * 
	 */
	public static final String MSG_PRE_EXCLUSION_LIST = "msg.pre.exclusion.list";
	
	/**
	 * 
	 */
	public static final String MSG_START = "msg.start";
	
	/**
	 * 
	 */
	public static final String MSG_END = "msg.end";
	
	/**
	 * 
	 */
	public static final String MSG_NB_FILE = "msg.nb.file";
	
	/**
	 * 
	 */
	public static final String MSG_JOB_FILE = "msg.job.file";
	
	/**
	 * 
	 */
	public static final String MSG_JOB_COPY = "msg.job.copy";
	
	/**
	 * 
	 */
	public static final String MSG_TAG_EXCLUDE = "msg.tag.exclude";
	
	/**
	 * 
	 */
	public static final String MSG_NO_TAG = "msg.no.tag";
	
	/**
	 * 
	 */
	public static final String POST_PARAMETER = "$";
	
	/**
	 * 
	 */
	public static final String FOLDER_SEPARATOR = "/";
	
	/**
	 * 
	 */
	public static final String ERROR_UNKNOW = "error.unknow";
	
	/**
	 * 
	 */
	public static final String ERROR_1x1 = "error.1x1";
	
	/**
	 * 
	 */
	public static final String ERROR_1x2 = "error.1x2";
	
	/**
	 * 
	 */
	public static final String ERROR_1x3 = "error.1x3";
		
	/**
	 * 
	 */
	public static final String ERROR_0x1 = "error.0x1";
	
	/**
	 * 
	 */
	public static final String ERROR_0x2 = "error.0x2";
}

/**
 * 
 */
package org.selfip.nibelungen.tagsortpictures.job.interfaces;

/**
 * @author Mickaël
 *
 */
public interface FolderTSPInterface {

	/**
	 * Erreur format ficher
	 */
	public static final String ERR_1x1 = "1x1";

	/**
	 * Erreur fichier inexistant
	 */
	public static final String ERR_1x2 = "1x2";
	
	/**
	 * Erreur fichier inexistant
	 */
	public static final String ERR_1x3 = "1x3";
	
	/**
	 *
	 */
	public static final String EXTENSION_SEPARATOR = ".";
	
	/**
	 * Type de tag resuperer
	 */
	public static final String[] FILTRE_IMG = {"jpg", "jpeg", "png"};
}

/**
 * 
 */
package org.selfip.nibelungen.tagsortpictures.job.interfaces;

/**
 * @author Mickaël
 *
 */
public interface PropertiesTSPInterface {
	
	/**
	 * 
	 */
	public static final String APPLICATION_PROPERTIES = "application.properties";
	
	/**
	 * 
	 */
	public static final String LANGUAGE_PROPERTIES = "org/selfip/nibelungen/tagsortpictures/languages/$1_tagsortpictures.properties";
}

/**
 * 
 */
package org.selfip.nibelungen.tagsortpictures.job;

/**
 * @author Mickaël
 *
 */
public enum MessageRowTSPEnum {
	/**
	 * 
	 */
	INFO, 
	
	/**
	 * 
	 */
	DEBUG, 
	
	/**
	 * 
	 */
	WARNING, 
	
	/**
	 * 
	 */
	ERROR;
}

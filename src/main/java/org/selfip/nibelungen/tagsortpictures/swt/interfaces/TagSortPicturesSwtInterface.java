/**
 * 
 */
package org.selfip.nibelungen.tagsortpictures.swt.interfaces;

import org.eclipse.swt.graphics.Color;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * @author Mickaël
 *
 */
public interface TagSortPicturesSwtInterface {

	/**
	 * 
	 */
	public static final Color GREEN = SWTResourceManager.getColor(152, 251, 152);

	/**
	 * 
	 */
	public static final Color RED = SWTResourceManager.getColor(250, 128, 114);

	/**
	 * 
	 */
	public static final Color ORANGE = SWTResourceManager.getColor(184, 134, 11);

	/**
	 * 
	 */
	public static final String ICO_TITLE = "/org/selfip/nibelungen/tagsortpictures/graphics/icon.png";
	
	/**
	 * 
	 */
	public static final String FOLDER_DEST_DEFAULT = "TagSortPictures";

}
